{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    just
    graphviz
    plantuml
    jdk
    jq
    minikube
    kubernetes-helm
    kubectl
    k9s
  ];
}
