# Wikifunction CI Helm Chart

This Helm chart deploys the WikiLambda CI environment on Kubernetes for testing
and development purposes. The environment includes a MediaWiki instance with the
WikiLambda extension, the services for Wikifunction, agnostic ingress routing.
It uses Bitnami's MariaDB.

## Installing locally on minikube
### Prerequisites

- [Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/) installed
- [Helm](https://helm.sh/docs/intro/install/) installed
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installed
- [k9s](https://k9ss.io) installed (optional, for monitoring the deployment)

### Getting Started

1. Make sure the Minikube cluster is up and running:

   ```
   minikube start
   ```
1. Install the NGINX ingress controller:

   ```
   minikube addons enable ingress
   ```

1. Set the kubectl context to Minikube:

   ```
   kubectl config use-context minikube
   ```

1. Add the Bitnami Helm repository:

   ```
   helm repo add bitnami https://charts.bitnami.com/bitnami
   ```

1. Install Bitnami's MariaDB:

   ```
   helm install mariadb bitnami/mariadb --set auth.rootPassword=secret
   ```

1. Get the Minikube IP:

   ```
   minikube ip
   ```

1. Set the `mediawikiUrl` in `values.yaml` using the Minikube IP and sslip.io:

   ```
   mediawikiUrl: <your-minikube-ip>.sslip.io
   ```

1. Deploy the WikiLambda CI environment using Helm:

   ```
   helm install wikilambda . -f values.yaml
   ```

1. Wait for the deployment to complete. You can optionally use k9s to monitor the progress:

   ```
   k9s
   ```

1. Once the deployment is complete, you can access the WikiLambda CI environment at:

   ```
   http://<your-minikube-ip>.sslip.io
   ```

### Running with k3s

If you are running under k3s, the steps differ slightly.

1. Make sure k3s is running.

1. Add the Bitnami Helm repository:

   ```
   helm repo add bitnami https://charts.bitnami.com/bitnami
   ```

1. Install Bitnami's MariaDB:

   ```
   helm install mariadb bitnami/mariadb --set auth.rootPassword=secret
   ```

1. Get your machine's private IP. This can be found with commands like `ip addr`.

1. Set the `mediawikiUrl` in `values.yaml` using the Minikube IP and sslip.io:

   ```
   mediawikiUrl: <your-private-ip>.sslip.io
   ```

1. Deploy the WikiLambda CI environment using Helm:

   ```
   helm install wikilambda . -f values.yaml
   ```

1. Wait for the deployment to complete. You can optionally use k9s to monitor the progress:

   ```
   k9s
   ```

1. Once the deployment is complete, you can access the WikiLambda CI environment at:

   ```
   http://<your-private-ip>.sslip.io
   ```

### Uninstalling the Chart

1. To uninstall the WikiLambda CI environment, run:

   ```
   helm uninstall wikilambda
   ```

1. Delete the MariaDB instance:

   ```
   helm uninstall mariadb -n wikilambda-ci
   ```

Please refer to the [Helm documentation](https://helm.sh/docs/) for more information on customizing your deployment and managing the release.
