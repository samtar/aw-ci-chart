# Wikifunctions CI Developer Handbook

## Evaluator configuration for the orchestrator
If you make changes to the evaluator deployments (e.g.: adding or removing an
evaluator, changing the supported languages), those changes must be reflected in
the orchestrator configuration.

### Purpose of the configuration
This configuration consists of a mapping between programming languages and evaluator
URIs. These URIs are identical to the names of the containers specified in
`templates/\<some-evaluator-image\>.yaml`. Programming language names can be any
valid `Z61` (see [the function model](https://meta.wikimedia.org/wiki/Abstract_Wikipedia/Pre-generic_function_model)) for details.

### Regenerating the configuration
There is a convenience script at
`./scripts/python/generate_environment_variables.py`. This script enshrines the
current source of truth for the orchestrator config. If you run this script with

```
python3 scripts/python/generate_environment_variables.py
```

some stringified JSON will be printed to the console. If you want to change the
configuration, simply alter the Python code and re-run the script. Then copy the
printed output to `template/function-orchestrator.yaml`, under the
`ORCHESTRATOR_CONFIG` environment variable.

## AppArmor
We use AppArmor to restrict the amount of mischief possible in native code. This
includes heavy restrictions to network and file system access.

### Installation
If AppArmor is not installed on your machine, please install it.

For Ubuntu/Debian users (including our CI machines themselves), this can be accomplished with

```
sudo apt install apparmor apparmor-utils
```

### AppArmor Profile Creation
You can (re)generate an AppArmor profile using the configuration profile provided here:

```
sudo apparmor_parser disallow-everything.apparmorprofile -r
```

This command will create a profile called `k8s-apparmor-disallow-everything` on
localhost, which is currently referred to in all of the evaluator Deployments.
